```bash
nix-shell -p nim pkgsCross.mingwW64.buildPackages.gcc
```

- `nix-shell` runs an interactive shell
- `-p` flag specifies which packages you want in the environment
	- the `--pure` flag can be passed which will clear the environment except for a few
- `pkgsCross.mingwW64.buildPackages.gcc` is the compiler that will be used by `nim`
	- if we wanted to target (i.e. build executables for) other host platforms, then we can find the specified packages with the following commands (assuming it’s supported by nim):
		- `nix repl '<nixpkgs>'`
		`pkgsCross.<press_TAB>`

---

We now need to tell Nim where gcc is located. In the shell run the following:

```bash
dirname $(which x86_64-w64-mingw32-gcc)
```

- `which x86…` reveals the full path to the executable
- we pass that to `dirname` using command substitution because we just want the directory that it’s in

---

In the src directory create a `nim.cfg` file:

```bash
nano nim.cfg
```

---

The contents of the file should something like:

```bash
amd64.windows.gcc.path = "/nix/store/dwzwjh68m86c8w8zfmkyws3cacankbdf-x86_64-w64-mingw32-stage-final-gcc-wrapper-12.2.0/bin"
```

`ctrl+s`, `ctrl+x` to exit.

---

Finally, compile your `.nim` file:

```bash
nim c -d:mingw --os:Windows <file_name>.nim
```

This should output `<file_name>.exe` in your current directory.

---

Check and make sure that it’s amd64:

```bash
file <file_name>.exe
```

---
